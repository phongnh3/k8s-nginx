FROM nginx

WORKDIR /root/

#ADD ./default.conf /etc/nginx/conf.d/default.conf

RUN apt-get update -y \
    && apt-get install -y htop \
    && apt-get install -y nano \
    && apt-get install -y net-tools \
    && apt-get install -y wget \
    && apt-get install -y telnet \
#    && wget https://wordpress.org/latest.tar.gz \
#    && tar xvaf latest.tar.gz \
    && rm -rf /var/lib/apt/lists/*

VOLUME [ "/etc/nginx/conf.d" ]

VOLUME [ "/var/www/html" ]

EXPOSE 80

ENTRYPOINT [ "/docker-entrypoint.sh" ]

CMD [ "nginx", "-g", "daemon off;" ]
